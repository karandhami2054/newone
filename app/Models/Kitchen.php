<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{
    protected $fillable = ['dish_name','dish_id','table_id','waiter_id','quantity','status','order_code', 'order_id'];

    public function dish_info(){
        return $this->hasOne('App\Models\Dish','id','dish_id');
    }

    public function getDishInfo(){
        return $this->with('dish_info')->get();
    }
}
