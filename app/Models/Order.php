<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['order_code','dish_id','table_id','waiter_id','quantity','unit_price','amount','status'];

    public function dish(){
        return $this->hasOne('App\Models\Dish','id','dish_id');
    }
    public function getOrderByTable($id){
        return $this->with('dish')->where('table_id', $id)->get();
    }
}
