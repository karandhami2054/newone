<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaiterNotification extends Model
{
    public function __construct()
    {
        $this->table = 'waiter_notifications';
    }

    protected $fillable = ['message'];
}
