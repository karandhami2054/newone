<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    // protected $table = "tables";
    protected $fillable = ['table_no','capacity','icon','waiter_id','status','occupied','floor_id'];

    public function getRules($options='add'){
        return [
            'table_no'=>'required|string',
            'capacity'=>'required|integer',
            'status'=>'required|in:active,inactive',
            'occupied'=>'required|in:occupied,reserved,open',
            'icon'=>'sometimes|image',
        ];
    }

}
