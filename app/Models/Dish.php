<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    
    protected $fillable =['title','slug','price','discount','act_price','summary','status','image','food_categories_id','added_by'];
    
    public function getRules($options='add'){
        return [
            'title'=>'required|string',
            'price'=>'required|integer',
            'discount'=>'sometimes|integer',
            'act_price'=>'sometimes|integer',
            'summary'=>'nullable|string',
            'status'=>'required|in:active,inactive',
            'image'=>'sometimes|image',
        ];
    }
}
