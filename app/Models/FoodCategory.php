<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    protected $fillable = ['title','slug','status','categories_id','added_by','image'];

    public function dishes(){
        return $this->hasMany('App\Models\Dish','food_categories_id','id')->orderBy('title','ASC');
    }

    public function getRules($options='add'){
        return [
            'title'=>'required|string',
            'status'=>'required|in:active,inactive',
            'image'=>'sometimes|image'
        ];
    }

}
