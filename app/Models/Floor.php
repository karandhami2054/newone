<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $fillable = ['name','slug','status','image'];

    public function tables(){
        return $this->hasMany('App\Models\Table','floor_id','id');
    }

    public function counts(){
        return $this->hasMany('App\Models\Table','floor_id','id')->where('occupied','!=','occupied');
    }

    public function getAllFloors(){
        return $this->with(['tables','counts'])->get();
    }

    public function getRules($options='add'){
        return [
            'name'=>'required|string',
            'status'=>'required|in:active,inactive',
            'image'=>'sometimes|image'
        ];
    }
}
