<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable = ['order_code','customer_id','waiter_id','sub_total','discount','service_charge','vat','grand_total','tips','payment_mode','other_payment_mode','cashier_id'];
}
