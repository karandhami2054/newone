<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title','slug','status','added_by'];

    public function food_categories(){
        return $this->hasMany('App\Models\FoodCategory','categories_id','id')->orderBy('title','ASC')->with('dishes');
    }
    public function getAllCategories(){
        return $this->with('food_categories')->get();
    }

    public function getRules($options='add'){
        return [
            'title'=>'required|string',
            'status'=>'required|in:active,inactive',
        ];
    }
}
