<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class OrderEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order_detail;
    protected $role;

    public function __construct($order_detail)
    {
        $this->order_detail = $order_detail;
        $this->role = $order_detail['role'];
    }

    public function broadcastOn()
    {
        return [new PrivateChannel('order.' . $this->role)];
    }

    public function broadcastAs()
    {
        return 'order-placed';
    }
    public function broadcastWith(){
        return ['order_detail' => $this->order_detail];
    }
}
