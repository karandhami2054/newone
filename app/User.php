<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRules($options='add'){
        return [
            'name'=>'required|string',
             'email'=>(($options == 'add') ? 'required' : 'nullable').'|email|'.(($options == 'add') ? 'unique:users,email' : ''),
           
             'password'=>(($options == 'add') ? 'required' : 'nullable').'|string',
            'status'=>'required|in:active,suspended',
            'role'=>'required|in:admin,cashier,waiter,kitchen,customer'
        ];
    }
}
