<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\FoodCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class FoodCategoryController extends Controller
{
    protected $foodCategory =null;
    protected $category =null;

    public function __construct(FoodCategory $foodCategory, Category $category){
        $this->foodCategory= $foodCategory;
        $this->category= $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= $this->foodCategory->orderBy('id','DESC')->get();
        return view('food_category.index')->with('data',$data);
    }

    public function admin()
    {
        $foodcategories = FoodCategory::all();
//        dd($category);
        return view('admin.foodcategory')->with('foodcategories',$foodcategories);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->category=$this->category->get();
        $return_category =array();
        foreach($this->category as $key=>$category){
            $return_category[$category->id]= $category->title;

        }

        return view('food_category.form')->with('category',$return_category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= $this->foodCategory->getRules();
        $request->validate($rules);
        $data=$request->all();

        $slug=\Str::slug($request->title);
        $found =$this->foodCategory->where('slug',$slug)->count();

        if($found > 0){
            $slug .=date('YmdHis');
        }
        $data['slug']= $slug;

        if($request->image){
            $path= public_path().('/images/foods');
            if(!File::exists($path)){
                File::makeDirectory($path,0777,true,true);
            }
            $filename = "Foods-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
            $request->image->move($path,$filename);
            $data['image'] =$filename;
        }
        $data['added_by'] =Auth::user()->id;
        $this->foodCategory =$this->foodCategory->fill($data);
        $success= $this->foodCategory->save();
        return redirect()->route('foodCategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->category=$this->category->get();
        $return_category =array();
        foreach($this->category as $key=>$category){
            $return_category[$category->id]= $category->title;

        }

        $this->foodCategory=$this->foodCategory->find($id);
        if(!$this->foodCategory){
            return redirect()->route('foodCategory.index');
        }
        return view('food_category.form')->with('edit_data',$this->foodCategory)->with('category',$return_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules= $this->foodCategory->getRules();
        $request->validate($rules);

        $data=$request->all();
        $this->foodCategory=$this->foodCategory->find($id);
        if(!$this->foodCategory){
            return redirect()->route('foodCategory.index');
        }

        if($request->image){
            $path= public_path().('/images/foods');

            $filename = "Foods-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
            $success= $request->image->move($path,$filename);
            if($success){
                $data['image'] =$filename;
                $old_image =$this->foodCategory->image;
                if($old_image !=null && file_exists(public_path().'/images/foods/'.$this->foodCategory->image)){
                    unlink(public_path().'/images/foods/'.$this->foodCategory->image);
                }
            }

        }

        $this->foodCategory=$this->foodCategory->fill($data);
        $this->foodCategory->save();
        return redirect()->route('foodCategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->foodCategory=$this->foodCategory->find($id);
        if(!$this->foodCategory){
            return redirect()->route('foodCategory.index');
        }

        $old_image =$this->foodCategory->image;
        $this->foodCategory->delete();
        if($old_image !=null && file_exists(public_path().'/images/foods/'.$this->foodCategory->image)){
            unlink(public_path().'/images/foods/'.$this->foodCategory->image);
        }
        return redirect()->route('foodCategory.index');
    }
    public function data()
    {
        $foodcategory = FoodCategory::all();
        return Datatables::of($foodcategory)->addColumn('action', function ($foodcategory) {
            return
                '<span>
                              <a href="' . route('foodCategory.edit', $foodcategory->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>'.
                '&nbsp;' .
                '<span>
                               <a href="' .route('foodCategoryDelete', $foodcategory->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);

    }
}
