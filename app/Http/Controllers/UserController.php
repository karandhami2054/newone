<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class UserController extends Controller
{
    protected $user =null;
    public function __construct(User $user){
        $this->user= $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= $this->user->where('id','!=',Auth::user()->id)->orderBy('id','DESC')->get();
        return view('user.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->user->getRules();
        $request->validate($rules);
        $data = $request->all();
        
        $data['password'] =Hash::make($request->password);
        $this->user =$this->user->fill($data);
        $success= $this->user->save();
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user=$this->user->find($id);
        if(!$this->user){
            return redirect()->route('user.index');
        }
        return view('user.form')->with('edit_data',$this->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules= $this->user->getRules('update');
        $request->validate($rules);
        $data=$request->all();
        $this->user=$this->user->find($id);
        if(!$this->user){
            return redirect()->route('user.index');
        }
        if(isset($data->password)){
            $data['password'] =Hash::make($request->password);
        }else{
            unset($data['password']);
        }
        $this->user=$this->user->fill($data);
        $this->user->save();
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::find($id);
        // $user->delete();
        // return redirect()->route('user.index');
        $this->user=$this->user->find($id);
        if(!$this->user){
            return redirect()->route('user.index');
        }
        $this->user->delete();
        return redirect()->route('user.index');
    }
    public function data()
    {
        $user = User::all();
        return Datatables::of($user)->addColumn('action', function ($user) {
            return
                '<span>
                              <a href="' . route('user.edit', $user->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>'.
                '&nbsp;' .
                '<span>
                               <a href="' .route('userDelete', $user->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);

    }
}
