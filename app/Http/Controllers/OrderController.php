<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use App\Models\Kitchen;
use App\Models\Order;
use App\Models\Table;
use App\Events\OrderEvent;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $dish = null;
    protected $order = null;
    protected $kitchen = null;

    public function __construct(Dish $dish, Order $order, Kitchen $kitchen)
    {
        $this->dish = $dish;
        $this->order = $order;
        $this->kitchen = $kitchen;
    }

    public  function orderDelivered($id){
        $this->order = $this->order->findOrFail($id);
        $this->order->update(['status' => 'served']);
    }

    public function createOrder(Request $request)
    {
        $this->dish = $this->dish->find($request->dish_id);
        if (!$this->dish) {
            return response()->json(['status' => false, 'msg' => "Dish not found.", 'data' => $request->all()]);
        }
        $table_id = $request->table_id;

        if ($this->order->where('table_id', $table_id)->first()) {
            $order_code = $this->order->where('table_id', $table_id)->take(1)->pluck('order_code')[0];
        } else {
            $order_code = \Str::random(15);
        }

        session()->put('order_code', $order_code);
        session()->put('table_id', $table_id);

        $new_qty = $request->quantity;

        $order_data = array(
            'dish_id' => $request->dish_id,
            'order_code' => $order_code,
            'status' => 'not_confirmed',
            'table_id' => $request->table_id,
            'waiter_id' => $request->user()->id,
            'unit_price' => $this->dish->price,
            'amount' => $this->dish->price * $new_qty,
            'quantity' => $new_qty
        );

        $this->order = $this->order->fill($order_data);
        $status = $this->order->save();
    }

    public
    function makePayment(Request $request)
    {
        $total = $this->order->where('order_code', $request->order_code)->sum('amount');
        $service_charge = $total * 0.10;
        $total += $service_charge;

        $vat = $total * 0.13;
        $total += $vat;

        $data = array(
            'order_code' => $request->order_code,
            'waiter_id', 'sub_total', 'discount', 'service_charge', 'vat', 'grand_total', 'tips', 'payment_mode', 'other_payment_mode', 'cashier_id');
        $id = $request->table_id;
        $table = Table::where('id', $id)->update(['occupied' => 'open']);
        $table1 = Order::where('table_id', $id)->delete();
        
        return redirect('/admin');
    }

    public
    function deleteOrder(Request $request)
    {
        $id = $request->id;
        if ($this->order = $this->order->where('id', $id)->first()) {
            if ($this->order->delete()) {
                return redirect()->back();
                return response()->json(['status' => true, 'msg' => 'Order deleted succesfully']);
            } else {
                return redirect()->back();
                return response()->json(['status' => false, 'msg' => 'Order could not be deleted at this moment']);
            }
        }
        return redirect()->back();
        return response()->json(['status' => false, 'msg' => 'Order could not be found..']);
    }

    public function OrderConfirmed($tableId)
    {
        if ($this->order->where('table_id', $tableId)->where('status', 'not_confirmed')->exists()) {
            foreach ($this->order->where('table_id', $tableId)->where('status', 'not_confirmed')->get(['id', 'dish_id', 'table_id', 'waiter_id', 'order_code', 'quantity']) as $order) {
                $order->update(['status' => 'confirmed']);
                $order = $order->toArray();
                $order['status'] = 'cook';
                $order['dish_name'] = Dish::where('id', $order['dish_id'])->value('title');
                $order['order_id'] = $order['id'];
                $kitchen = new Kitchen();
                $kitchen->fill($order);
                if ($kitchen->save()) {
                    $notification_data = [
                        'Dish_Name' => $kitchen->dish_name,
                        'Table_number' => $kitchen->table_id,
                        'link' => route('kitchen_status_change',$kitchen->id),
                        'quantity' => $kitchen->quantity,
                        'status' => 'cook',
                        'role' => 'kitchen',
                        'clear' => route('kitchenclear', $kitchen->dish_id)
                    ];
                    broadcast(new OrderEvent($notification_data));
                }
            }
            return response()->json(['status' => true, 'msg' => 'Order Placed successfully', 'data' => $this->order]);
        }
    }
}
