<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use App\Models\FoodCategory;
use App\Models\Category;
use App\Models\Order;
use App\Models\Table;
use App\User;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;


class TableController extends Controller
{
    protected $table_obj = null;
    protected $food_category = null;
    protected $category = null;
    protected $order = null;

    public function __construct(Table $table_obj, FoodCategory $food_category, Category $category, Order $order, Floor $floor, User $user)
    {
        $this->table_obj = $table_obj;
        $this->category = $category;
        $this->food_category = $food_category;
        $this->order = $order;
        $this->floor = $floor;
        $this->user = $user;

        $this->middleware(['auth']);
    }

    public function releaseTable(Request $request)
    {
        dd($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->table_obj = $this->table_obj->get();

        return view('admin.table')
            ->with('all_tables', $this->table_obj);
    }

    public function tableIndex()
    {
        $this->table_obj = $this->table_obj->orderBy('id', 'DESC')->get();

        return view('table.index')
            ->with('data', $this->table_obj);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->floor = $this->floor->get();
        $return_floor = array();
        foreach ($this->floor as $key => $floor) {
            $return_floor[$floor->id] = $floor->name;
        }
        $this->user = $this->user->where('role', 'waiter')->get();
        $return_waiter = array();
        foreach ($this->user as $key => $user) {
            $return_waiter[$user->id] = $user->name;
        }
        return view('table.form')->with('waiter', $return_waiter)->with('floor', $return_floor);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = $this->table_obj->getRules();
        $request->validate($rules);
        $data = $request->all();

        if ($request->icon) {
            $path = public_path() . ('/images/tables');
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $filename = "Tables-" . date('YmdHis') . rand(0, 100) . "." . $request->icon->getClientOriginalExtension();
            $request->icon->move($path, $filename);
            $data['icon'] = $filename;
        }

        $data['added_by'] = Auth::user()->id;


        $this->table_obj = $this->table_obj->fill($data);
        $success = $this->table_obj->save();
        return redirect()->route('tableIndex');
    }


    public function show($id)
    {
        $this->table_obj = $this->table_obj->find($id);
        if (!$this->table_obj) {
            return redirect()->back();
        }
        $this->category = $this->category->getAllCategories();
        $this->order = $this->order->getOrderByTable($id);

        return view('admin.table')
            ->with('categories', $this->category)
            ->with('orders', $this->order)
            ->with('table_data', $this->table_obj);
    }

    public function edit($id)
    {
        $this->floor = $this->floor->get();
        $return_floor = array();
        foreach ($this->floor as $key => $floor) {
            $return_floor[$floor->id] = $floor->name;
        }
        $this->user = $this->user->where('role', 'waiter')->get();
        $return_waiter = array();
        foreach ($this->user as $key => $user) {
            $return_waiter[$user->id] = $user->name;
        }

        $this->table_obj = $this->table_obj->find($id);
        if (!$this->table_obj) {
            return redirect()->route('tableIndex');
        }
        return view('table.form')->with('edit_data', $this->table_obj)->with('waiter', $return_waiter)->with('floor', $return_floor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->table_obj->getRules();
        $request->validate($rules);

        $data = $request->all();
        $this->table_obj = $this->table_obj->find($id);
        if (!$this->table_obj) {
            return redirect()->route('tableIndex');
        }


        if ($request->icon) {
            $old_image = $this->table_obj->icon;
            $path = public_path() . ('/images/tables');
            $filename = "tables-" . date('YmdHis') . rand(0, 100) . "." . $request->icon->getClientOriginalExtension();
            $success = $request->icon->move($path, $filename);
            if ($success) {
                $data['icon'] = $filename;
                if ($old_image != null && file_exists(public_path() . '/images/tables/' . $this->table_obj->icon)) {
                    unlink(public_path() . '/images/tables/' . $this->table_obj->icon);
                }
            }

        }

        $this->table_obj = $this->table_obj->fill($data);
        $this->table_obj->save();
        return redirect()->route('tableIndex');
    }


    public function destroy($id)
    {
        $this->table_obj = $this->table_obj->find($id);
        if (!$this->table_obj) {
            return redirect()->route('tableIndex');
        }
        $old_image = $this->table_obj->icon;
        if ($old_image != null && file_exists(public_path() . '/images/tables/' . $this->table_obj->icon)) {
            unlink(public_path() . '/images/tables/' . $this->table_obj->icon);
        }
        $this->table_obj->delete();
        return redirect()->route('tableIndex');
    }

    public function changeStatus(Request $request)
    {
        $this->table_obj = $this->table_obj->find($request->table_id);
        if (!$this->table_obj) {
            return response()->json(['status' => false, 'msg' => 'Table does not exists.', 'data' => null]);
        }
        $data = array(
            'occupied' => $request->status,
            'waiter_id' => $request->user()->id
        );
        // dd($data);

        $this->table_obj->fill($data);
        $status = $this->table_obj->save();
        if ($status) {
            return response()->json(['status' => true, 'msg' => 'Status changed successfully.', 'data' => null]);
        } else {
            return response()->json(['status' => false, 'msg' => 'Error changing status of table.', 'data' => null]);
        }
    }

    public function orderdataapi($id)
    {
        $orders = $this->order->getOrderByTable($id);
        $table_id = $id;
        // return response()->view("waiter._orderTable", compact('orders'));
        return response()->json(['status'=>true, 'html' => view('waiter._orderTable', compact('orders', 'table_id'))->render(), 'msg' => 'Order Table Reseted']);
    }

    public function tablereset($id)
    {
        $table = Table::where('id', $id)->update(['occupied' => 'open']);

    }

    public function table_show($id)
    {
        $this->table_obj = $this->table_obj->find($id);
        if (!$this->table_obj) {
            return redirect()->back();
        }
        $this->category = $this->category->getAllCategories();
        $this->order = $this->order->getOrderByTable($id);
        
        return view('waiter.table')
            ->with('categories', $this->category)
            ->with('orders', $this->order)
            ->with('table_data', $this->table_obj);
    }

    public function data()
    {
        $table = Table::all();
        return Datatables::of($table)->addColumn('action', function ($table) {
            return
                '<span>
                              <a href="' . route('table.edit', $table->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>' .
                '&nbsp;' .
                '<span>
                               <a href="' . route('tableDelete', $table->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);
    }
}
