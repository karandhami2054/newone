<?php

namespace App\Http\Controllers;

use App\Events\OrderComplete;
use App\Events\OrderCooked;
use App\Models\Kitchen;
use App\Models\Order;
use App\Models\WaiterNotification;
use Illuminate\Http\Request;

class KitchenController extends Controller
{
    protected $kitchen = null;

    public function __construct(Kitchen $kitchen)
    {
        $this->kitchen = $kitchen;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = $this->kitchen->getDishInfo();
//        $test = Order::all();
//        dd($orders);

        return view('kitchen.dashboard')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $orders=Kitchen::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function statuschange($id)
    {
        $this->kitchen = $this->kitchen->findOrFail($id);
        $status = ['cook', 'cooking', 'cooked'];

        if ($this->kitchen->status == 'cooked') {
            broadcast(new OrderCooked($this->kitchen, false))->toOthers();
            return response()->json(['status' => true, 'data' => $this->kitchen->status]);
        }
        $flag = $this->kitchen->update(['status' => $status[array_search($this->kitchen->status, $status) + 1]]);
        if ($this->kitchen->status == $status[2]) {
            $waiter_notify = new WaiterNotification();
            $waiter_notify->message = $this->kitchen->dish_name . ' is ready to be served on table ' . $this->kitchen->table_id;
            $waiter_notify->save();
            broadcast(new OrderCooked($this->kitchen))->toOthers();
        }
        if ($flag) {
            return response()->json(['status' => true, 'msg' => 'Status changed succesfully', 'data' => $this->kitchen->status]);
        }
        return response()->json(['status' => false, 'msg' => 'Unable to change status of the order']);

    }

    public function clear($id)
    {
        $clear = Kitchen::where('id', $id)->where('status', 'cooked')->delete();

        if ($clear) {
            return response()->json(['status' => true, 'msg' => 'The order is cleared']);
        }
        return response()->json(['status' => false, 'msg' => 'The order could not be cleard']);
    }

    public function notifier()
    {
        return view('kitchen.notificationtokitchen');

    }
}
