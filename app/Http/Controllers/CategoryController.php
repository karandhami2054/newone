<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class CategoryController extends Controller
{
    protected $category=null;

    public function __construct(Category $category)
    {
        $this->category=$category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= $this->category->orderBy('id','DESC')->get();
        return view('category.index')->with('data',$data);
    }

    public function admin()
    {
        $categories = Category::all();
//        dd($category);
        return view('admin.category')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= $this->category->getRules();
        $request->validate($rules);
        $data=$request->all();

        $slug=\Str::slug($request->title);
        $found =$this->category->where('slug',$slug)->count();

        if($found > 0){
            $slug .=date('YmdHis');
        }
        $data['slug']= $slug;

        $data['added_by'] =Auth::user()->id;
        $this->category =$this->category->fill($data);
        $success= $this->category->save();
        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->category=$this->category->find($id);
        if(!$this->category){
            return redirect()->route('category.index');
        }
        return view('category.form')->with('edit_data',$this->category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules= $this->category->getRules();
        $request->validate($rules);

        $data=$request->all();
        $this->category=$this->category->find($id);
        if(!$this->category){
            return redirect()->route('category.index');
        }

        $this->category=$this->category->fill($data);
        $this->category->save();
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->category=$this->category->find($id);
        if(!$this->category){

            return redirect()->route('category.index');
        }
        $this->category->delete();
        return redirect()->route('category.index');
    }

    public function data()
    {
        $category = Category::all();
        return Datatables::of($category)->addColumn('action', function ($category) {
            return
                '<span>
                              <a href="' . route('category.edit', $category->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>'.
                '&nbsp;' .
                '<span>
                               <a href="' .route('categoryDelete', $category->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);

    }
}
