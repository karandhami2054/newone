<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class FloorController extends Controller
{
    protected $floor =null;
    public function __construct(Floor $floor){
        $this->floor= $floor;
    }
    public function index()
    {
        $data= $this->floor->get();
        return view('floor.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('floor.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules= $this->floor->getRules();
        $request->validate($rules);
        $data=$request->all();
        $slug=\Str::slug($request->name);
        $found =$this->floor->where('slug',$slug)->count();

        if($found > 0){
            $slug .=date('YmdHis');
        }
        $data['slug']= $slug;
        if($request->image){
        $path= public_path().('/images/floors');
        if(!File::exists($path)){
            File::makeDirectory($path,0777,true,true);
        }
        $filename = "Floors-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
        $request->image->move($path,$filename);
        $data['image'] =$filename;
        }

        $this->floor =$this->floor->fill($data);
        $success= $this->floor->save();
        return redirect()->route('floor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->floor=$this->floor->find($id);
        if(!$this->floor){
            return redirect()->route('floor.index');
        }
        return view('floor.form')->with('edit_data',$this->floor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->floor=$this->floor->find($id);
        if(!$this->floor){
            return redirect()->route('floor.index');
        }
        $old_image =$this->floor->image;
        $rules= $this->floor->getRules('update');
        $request->validate($rules);
        $data=$request->all();
        if($request->image){
            $path= public_path().('/images/floors');

            $filename = "Floors-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
            $success= $request->image->move($path,$filename);
            if($success){
                $data['image'] =$filename;
                if($old_image !=null &&  file_exists(public_path().'/images/floors/'.$this->floor->image)){
                    unlink(public_path().'/images/floors/'.$this->floor->image);
                }
            }

        }
        $this->floor=$this->floor->fill($data);
        $this->floor->save();
        return redirect()->route('floor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->floor=$this->floor->find($id);
        if(!$this->floor){
            return redirect()->route('floor.index');
        }
        $old_image =$this->floor->image;
        if($old_image !=null &&  file_exists(public_path().'/images/floors/'.$this->floor->image)){
            unlink(public_path().'/images/floors/'.$this->floor->image);
        }

        $this->floor->delete();
        return redirect()->route('floor.index');
    }
    public function data()
    {
        $floor = Floor::all();
        return Datatables::of($floor)->addColumn('action', function ($floor) {
            return
                '<span>
                              <a href="' . route('floor.edit', $floor->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>'.
                '&nbsp;' .
                '<span>
                               <a href="' .route('floorDelete', $floor->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);

    }
}
