<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Dish;
use App\Models\FoodCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;


class ItemController extends Controller
{
    protected $dish=null;
    protected $food_category=null;
    public function __construct(Dish $dish, FoodCategory $food_category){
        $this->dish =$dish;
        $this->food_category =$food_category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->dish =$this->dish->orderBy('id','DESC')->get();
        return view('dishes.index')->with('data',$this->dish);
    }
    public function admin()
    {
        $items = Dish::all();
        $categories= Category::all();
//        dd($category);
        return view('admin.item')->with('items',$items)->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  $this->food_category =$this->food_category->get();
        $return_food= array();
        foreach($this->food_category as $key=>$value){
            $return_food[$value->id]= $value->title;
        }


        return view('dishes.form')->with('food_category',$return_food);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= $this->dish->getRules();
        $request->validate($rules);
        $data=$request->all();

        $slug=\Str::slug($request->title);
        $found =$this->dish->where('slug',$slug)->count();

        if($found > 0){
            $slug .=date('YmdHis');
        }
        $data['slug']= $slug;

        if($request->image){
            $path= public_path().('/images/foods');
            if(!File::exists($path)){
                File::makeDirectory($path,0777,true,true);
            }
            $filename = "foods-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
            $request->image->move($path,$filename);
            $data['image'] =$filename;
        }
        $data['added_by']= Auth::user()->id;
        $data['act_price'] =$request->price -$request->discount;
        $this->dish =$this->dish->fill($data);
        $success= $this->dish->save();
        return redirect()->route('item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->food_category =$this->food_category->get();
        $return_food= array();
        foreach($this->food_category as $key=>$value){
            $return_food[$value->id]= $value->title;
        }

        $this->dish =$this->dish->find($id);
        if(!$this->dish){
            return redirect()->route('item.index');
        }

        return view('dishes.form')->with('food_category',$return_food)->with('edit_data',$this->dish);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data=$request->all();

        $this->dish =$this->dish->find($id);
        if(!$this->dish){
            return redirect()->route('item.index');
        }
        $old_img =$this->dish->image;

        $data['slug']=\Str::slug($request->title);
        if($request->image){
            $path= public_path().('/images/foods');
            if(!File::exists($path)){
                File::makeDirectory($path,0777,true,true);
            }
            $filename = "foods-".date('YmdHis').rand(0,100).".".$request->image->getClientOriginalExtension();
            $success=$request->image->move($path,$filename);
            if($success){
                $data['image'] =$filename;
                if($old_img !=null && file_exists(public_path().'/images/foods/'.$old_img)){
                    unlink(public_path().'/images/foods/'.$old_img);
                }
            }

        }
        $data['act_price'] =$request->price -$request->discount;
        $this->dish =$this->dish->fill($data);
        $success= $this->dish->save();
        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dish=$this->dish->find($id);
        if(!$this->dish){
            return redirect()->route('item.index');
        }
        $old_image =$this->dish->image;
        $this->dish->delete();
        if($old_image !=null && file_exists(public_path().'/images/foods/'.$this->dish->image)){
            unlink(public_path().'/images/foods/'.$this->dish->image);
        }
        return redirect()->route('item.index');
    }
    public function data()
    {
        $dishes = Dish::all();
        return Datatables::of($dishes)->addColumn('action', function ($dishes) {
            return
                '<span>
                              <a href="' . route('item.edit', $dishes->id) . '">
                                   <button class="btn btn-warning">Edit</button>
                              </a>
                        </span>'.
                '&nbsp;' .
                '<span>
                               <a href="' .route('itemDelete', $dishes->id) .
                'onclick="return confirm("Are you sure you want to delete?")">' .
                '<button class="btn btn-danger">Delete</button>
                                                    </a>
                                                </span>';
        })->rawColumns(['action' => 'action'])
            ->make(true);
    }
}
