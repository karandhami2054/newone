<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use App\Models\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $floor = null;
    protected $order = null;

    /**
     * Create a new controller instance.
     *
     * @param Floor $floor
     */
    public function __construct(Floor $floor,Order $order)
    {
        $this->floor = $floor;
        $this->order = $order;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route(request()->user()->role);
    }

    public function admin(){
        $this->floor = $this->floor->getAllFloors();
        // dd($this->floor);

        return view('admin.dashboard')->with('floors',$this->floor);
    }

    public function cashier(){
        return view('home');
    }


    public function waiter(){
        $this->floor = $this->floor->getAllFloors();
        return view('waiter.dashboard')->with('floors',$this->floor);
    }

//    public function kitchen(){
//        $this->order = $this->order->where('status', 'confirmed')->orderBy('id','DESC')->get();
//        return view('kitchen.dashboard')->with('orders',$this->order);
//    }

    public function customer(){
        return view('home');
    }
    public function addData(){
        return view('admin.add-data');
    }
}
