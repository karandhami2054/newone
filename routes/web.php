<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {
    Route::put('table', 'TableController@changeStatus')->name('change-status');
    Route::post('order/{dish_id}', 'OrderController@createOrder')->name('order-dish');
    Route::get('orderkitchen', 'OrderController@createkitchenOrder')->name('order-dish-kitchen');
    Route::get('/apiorder/{id}', 'TableController@orderdataapi');
    Route::get('checkout/{order_code}/{table_id}', 'OrderController@makePayment')->name('make-payment');
    Route::get('/orderconfirmed/{tableId}', 'OrderController@OrderConfirmed')->name('order-confirmed');
    Route::get('/floordata', 'FloorController@data')->name('floordata');
    Route::get('/userdata', 'UserController@data')->name('userdata');
    Route::get('/categorydata', 'CategoryController@data')->name('categorydata');
    Route::get('/foodcategorydata', 'FoodCategoryController@data')->name('foodcategorydata');
    Route::get('/tabledata', 'TableController@data')->name('tabledata');
    Route::get('/itemdata', 'ItemController@data')->name('itemdata');

});
/******** Custom Routes Start ****************/
/********* Admin Routes Start ***********/
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'HomeController@admin')->name('admin');

    Route::get('/add-data', 'HomeController@addData')->name('addData');

    Route::resource('table', 'TableController');
    Route::get('/table', 'TableController@tableIndex')->name('tableIndex');
    Route::get('table-delete/{id}', 'TableController@destroy')->name('tableDelete');


    Route::resource('user', 'UserController');
    Route::get('/user-delete/{id}', 'UserController@destroy')->name('deleteUser');

    Route::resource('floor', 'FloorController');
    Route::get('floor-delete/{id}', 'FloorController@destroy')->name('floorDelete');

    Route::get('/release/{table_id}', 'TableController@releaseTable')->name('release-table');

    Route::get('/category', 'CategoryController@admin')->name('category');

    Route::resource('category', 'CategoryController');
    Route::get('category-delete/{id}', 'CategoryController@destroy')->name('categoryDelete');

    Route::resource('foodCategory', 'FoodCategoryController');
    Route::get('food-Category-Delete/{id}', 'FoodCategoryController@destroy')->name('foodCategoryDelete');

    Route::get('/item', 'ItemController@admin')->name('item');


    Route::resource('item', 'ItemController');
    Route::get('/item-delete/{id}', 'ItemController@destroy')->name('itemDelete');


    Route::get('/foodcategory', 'FoodCategoryController@admin')->name('foodcategory');

    Route::get('/floordata', 'FloorController@data')->name('floordata');
});
/********* Admin Routes Ends ***********/

/********* Cashier Routes Start ***********/
Route::group(['prefix' => 'cashier', 'middleware' => ['auth', 'cashier']], function () {
    Route::get('/', 'HomeController@cashier')->name('cashier');
});
/********* Cashier Routes Ends ***********/

/********* Waiter Routes Start ***********/
Route::group(['prefix' => 'waiter', 'middleware' => ['auth', 'waiter']], function () {
    Route::get('/', 'HomeController@waiter')->name('waiter');
    Route::get('/table/{id}', 'TableController@table_show')->name('table_show');


});
/********* Waiter Routes Ends ***********/

/********* Kitchen Routes Start ***********/
Route::group(['prefix' => 'kitchen', 'middleware' => ['auth', 'kitchen']], function () {
    Route::get('/', 'KitchenController@index')->name('kitchen');
//        Route::get('/','KitchenController@show')->name('kitchen');
        Route::get('/statuschange/{id}','KitchenController@statuschange')->name('kitchen_status_change');
    Route::get('/clear/{id}', 'KitchenController@clear')->name('kitchenclear');
    Route::get('/notificationfromkitchen', 'KitchenController@notifier')->name('notifier');

});
/********* Kitchen Routes Ends ***********/

/********* Customer Routes Start ***********/
Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'customer']], function () {
    Route::get('/', 'HomeController@customer')->name('customer');

});
/********* Customer Routes Ends ***********/

/******** Custom Routes Ends ****************/


/******************** Waiter And Admin Route *******************/
Route::post('/table/order', 'OrderController@createOrder')->name('add-order');
Route::get('/delete/order/{id}', 'OrderController@deleteOrder')->name('delete-order');
/******************** Waiter And Admin Route *******************/
