<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Please {{ isset($edit_data) ? 'update' : 'add'}} the form: </h2>
   
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if(isset($edit_data))
                <form action="{{ route('table.update',$edit_data->id) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                @else
              <form action="{{ route('table.store') }}" method="post" enctype="multipart/form-data">
              @endif
              @csrf
              <div class="form-group row">
                <label class="col-sm-3">Table number:  </label>
                  <div class="col-sm-9">
                    <input type="text" name="table_no" class="form-control form-control-sm" value="{{ isset($edit_data) ? @$edit_data->table_no : old('table_no')}}">
                    @if($errors->has('table_no'))  
                    <span class="alert-danger"> {{$errors->first('table_no')}} </span>   
                    @endif              
                  </div>
                </div>

                <div class="form-group row">
                <label class="col-sm-3">Capacity  </label>
                  <div class="col-sm-9">
                    <input type="number" name="capacity" class="form-control form-control-sm" value="{{ isset($edit_data) ? @$edit_data->capacity : old('capacity')}}">
                    @if($errors->has('capacity'))  
                    <span class="alert-danger"> {{$errors->first('capacity')}} </span>   
                    @endif              
                  </div>
                </div>

               
                <div class="form-group row">
                <label class="col-sm-3">Status:   </label>
                  <div class="col-sm-9">
                 <select name="status" id="status" class="form-control">
                 <option value="active"{{ (isset($edit_data) && ($edit_data->status=='active') ? 'selected' : '') }}>Active</option>
                    <option value="inactive"{{ (isset($edit_data) && ($edit_data->status=='inactive') ? 'selected' : '') }}>Inactive</option>
                 </select>
                    @if($errors->has('status'))  
                    <span class="alert-danger"> {{$errors->first('status')}} </span>   
                    @endif                   
                  </div>
                </div>

                <div class="form-group row">
                <label class="col-sm-3">Occupied:   </label>
                  <div class="col-sm-9">
                 <select name="occupied" id="occupied" class="form-control">
                 <option value="occupied"{{ (isset($edit_data) && ($edit_data->status=='occupied') ? 'selected' : '') }}>occupied</option>
                    <option value="reserved"{{ (isset($edit_data) && ($edit_data->status=='reserved') ? 'selected' : '') }}>reserved</option>
                    <option value="open"{{ (isset($edit_data) && ($edit_data->status=='open') ? 'selected' : '') }}>open</option>
                 </select>
                    @if($errors->has('occupied'))  
                    <span class="alert-danger"> {{$errors->first('occupied')}} </span>   
                    @endif                   
                  </div>
                </div>
               


               
                <div class="form-group row">
                                   {{ Form::label('waiter_id','Waiter_id: ',['class'=>'col-sm-3']) }}
                                    <div class="col-sm-9">
                                        {{Form::select('waiter_id',isset($waiter) ? $waiter : [],@$edit_data->waiter_id,['class'=>'form-control','id'=>'waiter_id','placeholder'=>'--Select any one--']) }}
                                        @if($errors->has('waiter_id'))
                                            <span class="alert-danger">
                                                {{ $errors->first('waiter_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                   {{ Form::label('floor_id','floor_id: ',['class'=>'col-sm-3']) }}
                                    <div class="col-sm-9">
                                        {{Form::select('floor_id',isset($floor) ? $floor : [],@$edit_data->floor_id,['class'=>'form-control','id'=>'floor_id','placeholder'=>'--Select any one--']) }}
                                        @if($errors->has('floor_id'))
                                            <span class="alert-danger">
                                                {{ $errors->first('floor_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                <div class="form-group row">
                <label class="col-sm-3">Icon:</label>
                  <div class="col-sm-4">
                    <input type="file" name="icon" class="form-control form-control-sm"  accept="image/*">
                    @if($errors->has('icon'))  
                    <span class="alert-danger"> {{$errors->first('icon')}} </span>   
                    @endif 
                    </div>      
                    @if(isset($edit_data) && file_exists(public_path().'/images/tables/'.$edit_data->icon))
                    <div class="col-sm-4">
                      <img style="max-width:150px;" src="{{ asset('images/tables/'.$edit_data->icon) }}" alt="no image found" class="img img-responsive img-thumbnail">
                    </div>
                    @endif
                                 
                 
                </div>


                <button class="btn-success" type="submit">Send</button>

                <button class="btn-danger" type="reset">Reset</button>

              </form>

            </div>
        </div>
    </div>

    
</body>
</html>