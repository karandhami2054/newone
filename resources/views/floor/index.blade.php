@extends('layouts.app')
@section('styles')
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(function() {
        $('#floor').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('floordata') }}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'status', name: 'status' },
                { data: 'action', name: 'action' }
            ]
        });
    });
</script>
    @endsection
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

@section('content')
    <a href="{{ route('floor.create')}}"><button class="btn btn-success float-right" >Add Data</button></a>
   <div class="container">
       <div class="row">
           <div class="col sm-12">
           <table border="1px" class="table table-striped" id="floor">
        <thead class="thead-dark">
            <th>Id</th>
            <th>Name</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
                        @foreach($data as $key=>$value)
                         <tr>
                             <td>{{$key+1}}</td>
                             <td>{{$value->name}}</td>
                             <td>{{$value->status}}</td>
                           
                             <td>
                                 <a class="btn btn-success" href="{{ route('user.edit',$value->id) }}">Edit</a>
                                 <a class="btn btn-success" href="{{ route('itemDelete',$value->id) }}" onclick="return confirm('Are you sure to delete')">Delete</a>
                                </td>
                         </tr>
                         @endforeach   
                    </tbody>




    </table>
           </div>
       </div>
   </div>

@endsection
