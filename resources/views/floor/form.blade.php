<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Please {{ isset($edit_data) ? 'update' : 'add'}} the form: </h2>
   
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if(isset($edit_data))
                <form action="{{ route('floor.update',$edit_data->id) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                @else
              <form action="{{ route('floor.store') }}" method="post" enctype="multipart/form-data">
              @endif
              @csrf
              <div class="form-group row">
                <label class="col-sm-3">Name:  </label>
                  <div class="col-sm-9">
                    <input type="text" name="name" class="form-control form-control-sm" value="{{ isset($edit_data) ? @$edit_data->name : old('name')}}">
                    @if($errors->has('name'))  
                    <span class="alert-danger"> {{$errors->first('name')}} </span>   
                    @endif              
                  </div>
                </div>

                <div class="form-group row">
                <label class="col-sm-3">Status:   </label>
                  <div class="col-sm-9">
                 <select name="status" id="status" class="form-control">
                 <option value="active"{{ (isset($edit_data) && ($edit_data->status=='active') ? 'selected' : '') }}>Active</option>
                    <option value="inactive"{{ (isset($edit_data) && ($edit_data->status=='inactive') ? 'selected' : '') }}>Inactive</option>
                
                 </select>
                    @if($errors->has('status'))  
                    <span class="alert-danger"> {{$errors->first('status')}} </span>   
                    @endif                   
                  </div>
                </div>

                <div class="form-group row">
                <label class="col-sm-3">Image:</label>
                  <div class="col-sm-4">
                    <input type="file" name="image" class="form-control form-control-sm"  accept="image/*">
                   
                    @if($errors->has('image'))  
                    <span class="alert-danger"> {{$errors->first('image')}} </span>   
                    @endif 
                    </div>      
                   
                    @if(isset($edit_data) && file_exists(public_path().'/images/floors/'.$edit_data->image))
                    <div class="col-sm-4">
                      <img style="max-width:150px;" src="{{ asset('images/floors/'.$edit_data->image) }}" alt="no image found" class="img img-responsive img-thumbnail">
                    </div>
                    @endif
                                 
                 
                </div>

             

                <button class="btn-success" type="submit">Send</button>

                <button class="btn-danger" type="reset">Reset</button>

              </form>

            </div>
        </div>
    </div>

    
</body>
</html>