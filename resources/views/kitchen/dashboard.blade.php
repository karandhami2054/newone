@extends('layouts.app')
@section('scripts')
    <script>
        $('.table').on('click', '#clearKitchenOrder', function (event) {
            event.preventDefault();
            let el = $(this).closest('tr');
            $.ajax({
                url: $(this).attr('href'),
                type: "get",
                success: function (response) {
                    if (response.status == true) {
                        el.remove();
                    }
                }
            })
        });

        $('.table').on('click', '#orderStatusChange', function (event) {
            event.preventDefault();
            let el = $(this);
            $.ajax({
                url: $(this).attr('href'),
                type: "get",
                success: function (response) {
                    if (response.status == true) {
                        if (response.data == 'cooked') {
                            el.parent().parent().find('#clearKitchenOrder').removeClass('disabled');
                        }
                        console.log(response.data);
                        el.html(response.data);
                    }
                }
            })
        });

        function addOrderToTable(data) {
            let html = `<tr>
            <td>${data.Dish_Name}</td>
            <td>${data.quantity}</td>
            <td>${data.Table_number}</td>
            <td>
            <a href="${data.link}"
                                   class="btn btn-primary" id="orderStatusChange">${data.status}</a>
            </td>
            <td><a href="${data.clear}" class="btn btn-danger disabled" id="clearKitchenOrder">clear</a></td>
            </tr>`;
            $('table').find('tbody').append(html);
        }

        Echo.private('order.{{ auth()->user()->role }}')
            .listen('.order-placed', function (response) {
                let data = response.order_detail;
                addOrderToTable(data);
            });
    </script>
@endsection
@section('content')
    {{--    {{ dd(request()) }}--}}
    <div class="row">
        <div class="col-12">
            <table class="table table-striped">
                <thead class="thead-dark">
                <th>Particular</th>
                <th>Quantity</th>
                <th>Table</th>
                <th>Status</th>
                <th>Action</th>

                </thead>
                <tbody>
                @php
                    $sub_total = 0;
                @endphp
                @forelse($orders as $key => $value)
                    <tr>
                        <td>{{$value->dish_name}}</td>

                        <td>{{ $value->quantity }}</td>
                        <td>{{ $value->table_id }}</td>
                        <td>
                            <a href="{{ route('kitchen_status_change',$value->id) }}"
                               class="btn btn-primary" id="orderStatusChange">{{$value->status}}</a>

                        </td>
                        <td><a href="{{route('kitchenclear',$value->id) }}"
                               class="btn btn-danger {{ $value->status == 'cooked' ? '' : 'disabled' }}"
                               id="clearKitchenOrder">clear</a></td>
                    </tr>
                @empty
                @endforelse
                </tbody>
                <tfoot>

                </tfoot>
            </table>
            <br>
        </div>
    </div>
@endsection
