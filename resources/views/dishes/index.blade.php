@extends('layouts.app')
@section('content')


    @section('styles')
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    @endsection
    @section('scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('itemdata') }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'price', name: 'price' },
                    { data: 'discount', name: 'discount' },
                    { data: 'act_price', name: 'act_price' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
    @endsection
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <a href="{{ route('item.create')}}"><button class="btn btn-success float-right" >Add Data</button></a>
   <div class="container">
       <div class="row">
           <div class="col sm-14">
           <table border="1px" class="table table-striped" id="item">
        <thead class="thead-dark">
            <th>Id</th>
            <th>Title</th>
            <th>Price (Rs.)</th>
            <th>Discount (Rs.)</th>
            <th>Actual Price (Rs.)</th>
            <th>Action</th>
        </thead>
        <tbody>
                        @foreach($data as $key=>$value)
                         <tr>
                             <td>{{$key+1}}</td>
                             <td>{{$value->title}}</td>
                             <td>{{$value->price}}</td>
                             <td>{{$value->discount}}</td>
                             <td>{{$value->act_price}}</td>
                             <td>
                                 <a class="btn btn-success" href="{{ route('user.edit',$value->id) }}">Edit</a>
                                 <a class="btn btn-success" href="{{ route('itemDelete',$value->id) }}" onclick="return confirm('Are you sure to delete')">Delete</a>
                                </td>
                         </tr>
                         @endforeach   
                    </tbody>


    </table>
           </div>
       </div>
   </div>
</body>
@endsection
