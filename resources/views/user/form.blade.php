<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User add form</title>
</head>
<body>
    <h2>Please {{ isset($edit_data) ? 'update' : 'add'}} the form: </h2>
   
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if(isset($edit_data))
                <form action="{{ route('user.update',$edit_data->id) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                @else
              <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
              @endif
              @csrf
              <div class="form-group row">
                <label class="col-sm-3">Name  </label>
                  <div class="col-sm-9">
                    <input type="text" name="name" class="form-control form-control-sm" value="{{ isset($edit_data) ? @$edit_data->name : old('name')}}">
                    @if($errors->has('name'))  
                    <span class="alert-danger"> {{$errors->first('name')}} </span>   
                    @endif              
                  </div>
                </div>

               
<div class="form-group row" style="display: {{ isset($edit_data) ? 'none' : 'hidden'}}" >                      
                <label class="col-sm-3">Email:</label>
                  <div class="col-sm-9">
                    <input type="email" name="email" class="form-control form-control-sm" value="{{  isset($edit_data) ? @$edit_data->email : old('email')}}">
                    @if($errors->has('email'))  
                    <span class="alert-danger"> {{$errors->first('email')}} </span>   
                    @endif                     
                  </div>
                </div>

               <div class="form-group row" style="display: {{ isset($edit_data) ? 'none' : 'hidden'}}"  >
                <label class="col-sm-3">Password:</label>
                  <div class="col-sm-9">
                    <input type="text" name="password" class="form-control form-control-sm" >    
                    @if($errors->has('password'))  
                    <span class="alert-danger"> {{$errors->first('password')}} </span>   
                    @endif                 
                  </div>
                </div>

                <div class="form-group row" >
                <label class="col-sm-3">Role:</label>
                  <div class="col-sm-9">
                 <select name="role" id="role" class="form-control">
                   <option value="admin" {{ (isset($edit_data) && ($edit_data->role=='admin') ? 'selected' : '') }}>Admin</option>
                   <option value="cashier"{{ (isset($edit_data) && ($edit_data->role=='cashier') ? 'selected' : '') }}>Cashier</option>
                   <option value="waiter"{{ (isset($edit_data) && ($edit_data->role=='waiter') ? 'selected' : '') }}>Waiter</option>
                   <option value="kitchen"{{ (isset($edit_data) && ($edit_data->role=='kitchen') ? 'selected' : '') }}>Kitchen</option>
                   <option value="customer"{{ (isset($edit_data) && ($edit_data->role=='customer') ? 'selected' : '') }}>Customer</option>
                 </select>
                  @if($errors->has('role'))  
                    <span class="alert-danger"> {{$errors->first('role')}} </span>   
                    @endif                 
                  </div>
                </div>

                <div class="form-group row" >
                <label class="col-sm-3">Status:</label>
                  <div class="col-sm-9">
                 
                  <select name="status" id="status" class="form-control">
                    <option value="active"{{ (isset($edit_data) && ($edit_data->status=='active') ? 'selected' : '') }}>Active</option>
                    <option value="suspended"{{ (isset($edit_data) && ($edit_data->status=='suspended') ? 'selected' : '') }}>Suspended</option>
                  </select>    
                    @if($errors->has('status'))  
                    <span class="alert-danger"> {{$errors->first('status')}} </span>   
                    @endif                 
                  </div>
                </div>

             

                <button class="btn-success" type="submit">Send</button>

                <button class="btn-danger" type="reset">Reset</button>

              </form>

            </div>
        </div>
    </div>

    
</body>
</html>