@extends('layouts.app')
@section('styles')
    <style>
        .table-wrapper {
            border: 1px solid #cccccc;
            margin-left: 10px;
            padding: 2% 0% 2% 5%;
            border-radius: 28%;
            margin-top: 1%;
        }
        .occupied{
            background: #ffc3c354;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                @if($floors)
                    @foreach($floors as $items)
                        <a class="nav-item nav-link {{ $loop->first ? 'active' : '' }}" id="nav-{{ $items->slug }}-tab" data-toggle="tab" href="#nav-{{ $items->slug }}" role="tab" aria-controls="nav-{{ $items->slug }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">
                            {{ $items->name }} ({{ $items->counts->count() }})
                        </a>
                    @endforeach
                @endif
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            @if($floors)
                @foreach($floors as $items)
                    <div class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="nav-{{ $items->slug }}" role="tabpanel" aria-labelledby="nav-{{ $items->slug }}-tab">
                            <div class="row">
                            @if($items->tables->count() > 0)
                                @foreach($items->tables as $table_info)
                                    <div class="col-2 table-wrapper {{ $table_info->occupied }}">
                                        <i class="fa fa-table fa-3x" ></i> {{ $table_info->table_no }}
                                        <br>
                                        @if($table_info->capacity)
                                            @for($i = 1; $i <= $table_info->capacity; $i++)
                                                <i class="fa fa-chair"></i>C {{ $i }}
                                            @endfor
                                        @endif
                                        <div class="button-wrapper">
                                            <button data-status="occupied" class="btn btn-success change-status {{ ($table_info->occupied =='occupied') ? 'd-none' : '' }}"  data-id="{{ $table_info->id }}">
                                                Occupy
                                            </button>
                                            <a href="{{ route('table.show',$table_info->id) }}" class="btn btn-success">
                                                View
                                            </a>

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

@endsection
@section('scripts')
    <script>
        $('.change-status').click(function(){
            var table_id = $(this).data('id');
            var status = $(this).data('status');
            var elem = $(this);
            $.ajax({
               url: "{{ route('change-status') }}",
               type: "post",
               data:{
                   _method: 'put',
                   _token: "{{ csrf_token() }}",
                   datatype: "jsonp",
                   table_id: table_id,
                   status: status
               },
                success: function(response){
                    if(typeof(response) != 'object'){
                        response = $.parseJSON(response);
                    }
                    if(response.status){
                        document.location.href ="{{ url('/admin/table/') }}/"+table_id
                    }
                }
            });
            $(this).addClass('occupied');
        });

    </script>
@endsection
