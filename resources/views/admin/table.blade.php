@extends('layouts.app')
@section('styles')
    <style>
        .table-wrapper {
            border: 1px solid #cccccc;
            margin-left: 10px;
            padding: 2% 0% 2% 5%;
            border-radius: 28%;
            margin-top: 1%;
        }
        li.dish-list {
            list-style: none;
            float: left;
            border: 5px solid #00ff00;
            padding: 5px;
            margin: 5px;
            border-radius: 6px;
            background: #edffed;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $('.release_btn').click(function(){
            var table_id = $(this).data('id');
            $.ajax({
               url: "{{ url('/admin/release/') }}/"+table_id,
               type: "get",
                success: function(response){
                   console.log(response);
                }
            });
        });

        function addOrder(elem){
            let dish_id = $(elem).data('id');
            $('#order-form').removeClass('d-none');
            $('#form_order').attr('action',"{{ url('order/') }}/"+dish_id);
        }

        $('#form_order').on('submit',function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type:"post",
                data: $('#form_order').serialize(),
                success: function(response){
                    console.log(response);
                }
            });

        });

        $(document).ready(function() {
            var obj = JSON.parse({{$table_data->id}});
            $.ajax({
                url: '/apiorder/' + obj,
                type: 'get',
                dataType: 'JSON',
                success: function (response) {
                    var html = '';
                    var subtotal = 0;

                    if (typeof (response) != "object") {
                        response = $.parseJSON(response);
                    }
                    $.each(response, function (key, value) {
                        html += '<tr>';
                        html += '<td>' + (value.dish.title) + '</td>';
                        html += '<td>' + value.quantity + '</td>';
                        html += '<td>' + value.unit_price + '</td>';
                        html += '<td>' + (value.unit_price * value.quantity) + '</td>';
                        if(value.status=="not_confirmed") {
                            html += '<td><a href="/admin/delete/' + value.id + '/' + value.table_id + '" class="btn btn-danger" onclick="return confirm(\'Delete Confirmation\')">DeleteOrder</a>';
                        }
                            html += '</tr>';
                        subtotal += value.quantity * value.unit_price;
                    });

                    var service_charge = parseFloat(subtotal * 0.10).toFixed(2);
                    var VAT = Math.round(subtotal * 0.13, 2);
                    var total = Math.round((parseFloat(subtotal) + parseFloat(service_charge) + parseFloat(VAT)), 2);

                    $('#tbody').html(html);
                    $('#sub_total').html("Npr." + Math.round(subtotal, 2));
                    $('#service_charge').html("Npr." + service_charge);
                    $('#vat').html("Npr." + VAT);
                    $('#total').html("Npr." + total);
                },
            });
        });

        $('#form_order').on('submit', function (e) {
            var obj = JSON.parse({{$table_data->id}});
            e.preventDefault();

            $.ajax({
                url: '/apiorder/' + obj,
                type: 'get',
                dataType: 'JSON',
                success: function (response) {
                    var html = '';
                    var subtotal = 0;

                    if (typeof (response) != "object") {
                        response = $.parseJSON(response);
                    }$.each(response, function (key, value) {
                        html += '<tr>';
                        html += '<td>' + (value.dish.title) + '</td>';
                        html += '<td>' + value.quantity + '</td>';
                        html += '<td>' + value.unit_price + '</td>';
                        html += '<td>' + (value.unit_price * value.quantity) + '</td>';
                        if(value.status=="not_confirmed") {
                            html += '<td><a href="/admin/delete/' + value.id + '/' + value.table_id + '" class="btn btn-danger" onclick="return confirm(\'Delete Confirmation\')">DeleteOrder</a>';
                        }
                        html += '</tr>';
                        subtotal += value.quantity * value.unit_price;
                    });
                    var service_charge = parseFloat(subtotal * 0.10).toFixed(2);
                    var VAT = Math.round(subtotal * 0.13,2);
                    var total = Math.round((parseFloat(subtotal) + parseFloat(service_charge) + parseFloat(VAT)),2);

                    $('#tbody').html(html);
                    $('#sub_total').html("Npr." + Math.round(subtotal,2));
                    $('#service_charge').html("Npr." + service_charge);
                    $('#vat').html("Npr." + VAT);
                    $('#total').html("Npr." + total);

                },

            });
        });


    </script>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-4">
                <h4 class="text-center">Table Data</h4>
            </div>
            <div class="col-9">
                {{--<a href="javascript:;" class="btn btn-success release_btn" data-id="{{ $table_data->id }}">
                    Release
                </a>
                <a href="" class="btn btn-success">
                    Switch
                </a>--}}
            </div>

        </div>
        <div class="row">
            <div class="col-5">
                <h4>Order Info</h4>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <th>Particular</th>
                                <th>Quantity</th>
                                <th>Price(Npr.)</th>
                                <th>Amount(Npr.)</th>
                                <th>Action</th>
                            </thead>
                            <tbody id="tbody">
                                @php
                                    $sub_total = 0;
                                @endphp
                                @if($orders->count())

                                @else
                                    <tr>
                                        <th colspan="4" class="text-center">No Dish Added</th>
                                    </tr>
                                @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                {{--                                    @if($sub_total)--}}
                                <th colspan="3" class="text-right" >Sub-Total: </th>
                                <th id="sub_total"></th>
                                {{--                                    @endif--}}
                            </tr>
                            <tr>
                                <th colspan="3" class="text-right" >Service Charge(10%): </th>
                                <th id="service_charge"></th>
                            </tr>
                            {{--<tr>
                                <th class="text-right" colspan="3">Discount(5%)</th>
                                <th>Npr.
                                    @php
                                        $discount = $sub_total*0.05;
                                        $sub_total = $sub_total-$discount;
                                    @endphp
                                    {{ number_format($discount,2) }}</th>
                            </tr>--}}
                            <tr>
                                <th colspan="3" class="text-right" >VAT (13%): </th>
                                <th id="vat"></th>

                                </th>
                            </tr>

                            <tr>
                                <th colspan="3" class="text-right" >Total: </th>
                                <th id="total"></th>
                                </th>
                            </tr>

                            </tfoot>
                        </table>

                        <br>
                        <div id="Confirm">

                        </div>
                        @if($orders->count())
                        <a href="{{route('order-confirmed',$orders[0]->order_code)}}" class="btn btn-warning">Confirm Order</a>


                            <a href="{{ route('make-payment', [$orders[0]->order_code,$table_data->id]) }}" class="btn btn-success">Pay</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-6">
                <h4>Menu</h4>
                <hr>
                @if($categories)
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @foreach($categories as $cat_info)
                            <li class="nav-item">
                                <a class="nav-link {{ $loop->first ? 'active' :'' }}" id="{{$cat_info->slug}}-tab" data-toggle="tab" href="#{{ $cat_info->slug }}" role="tab" aria-controls="{{ $cat_info->slug }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">
                                    {{ $cat_info->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif


                <div class="tab-content" id="myTabContent">
                    @if($categories)
                        @foreach($categories as $cat_info)
                            <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $cat_info->slug }}" role="tabpanel" aria-labelledby="{{ $cat_info->slug }}-tab">
                                @if($cat_info->food_categories)
                                    <div class="row">
                                        <div class="col-3 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        @foreach($cat_info->food_categories as $food_cats)
                                            <a class="nav-link {{ $loop->first ?'active':'' }}" id="v-pills-{{ $food_cats->slug }}-tab" data-toggle="pill" href="#v-pills-{{ $food_cats->slug }}" role="tab" aria-controls="v-pills-{{ $food_cats->slug }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $food_cats->title }}</a>
                                        @endforeach
                                        </div>
                                        <div class="col-9 tab-content" id="v-pills-tabContent">
                                            @foreach($cat_info->food_categories as $food_cats)
                                                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="v-pills-{{ $food_cats->slug }}" role="tabpanel" aria-labelledby="v-pills-{{ $food_cats->slug }}-tab">
                                                    @if($food_cats->dishes)
                                                        <ul>
                                                            @foreach($food_cats->dishes as $dish_item)
                                                                 <li class="dish-list" onclick="addOrder(this)" data-id="{{ $dish_item->id }}">
                                                                     <h6>{{ $dish_item->title }}</h6>
                                                                     <p>NPR. {{ number_format($dish_item->price) }}</p>
                                                                 </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
                
                <div class="row d-none" id="order-form">
                    <div class="col-12">
                        <hr>
                        <h5 class="text-center">Order Form</h5>
                        <hr>

                        {{ Form::open(['id'=>'form_order','url'=>'']) }}
                        <div class="form-group row">
                            {{ Form::label('quantity',"Quantity: ",['class'=>'col-sm-4']) }}
                            <div class="col-sm-4">
                                </span>
                                <input type="number" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                            </div>
                            <div class="col-sm-4">
              </button>
                                {{ Form::hidden('table_id',$table_data->id) }}
                                {{ Form::button('Add Order',['class'=>'btn btn-success', 'id'=>'place_order','type'=>'submit']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
