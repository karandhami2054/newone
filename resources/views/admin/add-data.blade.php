@extends('layouts.app')
@section('content')
<body>

<div class="container">
    <br><br>
    <div class="row">

        <div class="col-sm-12">
            <a class="btn btn-success" href="{{ route('user.index') }}">
                User
            </a>
            <a class="btn btn-success" href="{{ route('floor.index') }}">
                Floor
            </a>

            <a class="btn btn-success" href="{{ route('category.index') }}">
                Category
            </a>

            <a class="btn btn-success" href="{{ route('foodCategory.index') }}">
                Food Category
            </a>
            <a class="btn btn-success" href="{{ route('tableIndex') }}">
                Table
            </a>

            <a class="btn btn-success" href="{{ route('item.index') }}">
                Dishes
            </a>
        </div>

    </div>
</div>

</body>
@endsection


