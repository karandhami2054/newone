<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Please {{ isset($edit_data) ? 'update' : 'add'}} the form: </h2>
   
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if(isset($edit_data))
                <form action="{{ route('category.update',$edit_data->id) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                @else
              <form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
              @endif
              @csrf
              <div class="form-group row">
                <label class="col-sm-3">Title:  </label>
                  <div class="col-sm-9">
                    <input type="text" name="title" class="form-control form-control-sm" value="{{ isset($edit_data) ? @$edit_data->title : old('title')}}">
                    @if($errors->has('title'))  
                    <span class="alert-danger"> {{$errors->first('title')}} </span>   
                    @endif              
                  </div>
                </div>

               

                <div class="form-group row">
                <label class="col-sm-3">Status:   </label>
                  <div class="col-sm-9">
                 <select name="status" id="status" class="form-control">
                 <option value="active"{{ (isset($edit_data) && ($edit_data->status=='active') ? 'selected' : '') }}>Active</option>
                    <option value="inactive"{{ (isset($edit_data) && ($edit_data->status=='inactive') ? 'selected' : '') }}>Inactive</option>
                
                 </select>
                    @if($errors->has('status'))  
                    <span class="alert-danger"> {{$errors->first('status')}} </span>   
                    @endif                   
                  </div>
                </div>


             

                <button class="btn-success" type="submit">Send</button>

                <button class="btn-danger" type="reset">Reset</button>

              </form>

            </div>
        </div>
    </div>

    
</body>
</html>