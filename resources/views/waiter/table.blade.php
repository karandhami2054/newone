@extends('layouts.app')
@section('styles')
    <style>
        .table-wrapper {
            border: 1px solid #cccccc;
            margin-left: 10px;
            padding: 2% 0% 2% 5%;
            border-radius: 28%;
            margin-top: 1%;
        }

        li.dish-list {
            list-style: none;
            float: left;
            border: 5px solid #00ff00;
            padding: 5px;
            margin: 5px;
            border-radius: 6px;
            background: #edffed;
        }

        .selected_dish {
            background-color: #73e831 !important;
        }
    </style>
@endsection
@section('scripts')
    <script>
        const TABLE_ID = '{{ $table_data->id }}';

        Echo.private('order.waiter')
            .listen('.order-cooked', function (response) {
                let detail = response.detail;
                if (response.updateNotification === true) {
                    let html = `<span class="dropdown-item"><i class="fa fa-arrow-right pr-2"></i>${detail.dish_name} is ready to be served on table ${detail.table_id}</span>
                                        <div class="dropdown-divider"></div>`;
                    $('nav #notification_list').append(html);
                }
                if (detail.waiter_id == parseInt({{ auth()->user()->id }})) {
                    confirm(`${detail.dish_name} is ready to be served on table ${detail.table_id}`)
                }
            });

        $('.release_btn').click(function () {
            $.ajax({
                url: "{{ url('/admin/release/') }}/" + TABLE_ID,
                type: "get",
                success: function (response) {
                    console.log(response);
                }
            });
        });

        function addOrder(elem) {
            let dish_id = $(elem).data('id');
            $('.dish-list').each(function () {
                $(this).removeClass('selected_dish');
            });
            $('#order-form').removeClass('d-none');
            $(elem).addClass('selected_dish');
            $('#form_order').attr('action', "{{ url('order/') }}/" + dish_id);
        }

        $(document).ready(function () {
            getOrderData(TABLE_ID);
        });

        $('#form_order').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                data: $(this).serialize(),
                success: function (response) {
                    $('#order-form').addClass('d-none');
                    getOrderData(TABLE_ID);
                }
            });
        });

        function getOrderData(id) {
            $.ajax({
                url: '/apiorder/' + parseInt(id),
                type: 'get',
                success: function (response) {
                    if (response.status == true) {
                        document.querySelector('.table_content').innerHTML = (response.html);
                    }
                }
            });
        }

        function orderStatusChange($id) {
            $.ajax({
                url: url('')
            })
        }

        $('.confirm_order').on('click', function (event) {
            event.preventDefault();
            $.ajax({
                url: `/orderconfirmed/${TABLE_ID}`,
                type: 'get',
                success: function (response) {
                    if (response.status == true) {
                        getOrderData(TABLE_ID);
                    } else {
                        console.log(response);
                    }
                }
            });
        });
    </script>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-3">
                <h4 class="text-center">Table Data</h4>
            </div>
          
        </div>
        <div class="row">
            <div class="col-6">
                <h4>Order Info</h4>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="table_content"></div>
                        <br>
                        <a class="btn btn-warning confirm_order">Confirm Order</a>
                        @if($orders->count())
                            <a href="{{ route('make-payment', [$orders[0]->order_code,$table_data->id]) }}"
                               class="btn btn-success">Pay</a>

                        @endif

                    </div>
                </div>
            </div>
            <div class="col-6">
                <h4>Menu</h4>
                <hr>
                @if($categories)
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @foreach($categories as $cat_info)
                            <li class="nav-item">
                                <a class="nav-link {{ $loop->first ? 'active' :'' }}" id="{{$cat_info->slug}}-tab"
                                   data-toggle="tab" href="#{{ $cat_info->slug }}" role="tab"
                                   aria-controls="{{ $cat_info->slug }}"
                                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">
                                    {{ $cat_info->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif


                <div class="tab-content" id="myTabContent">
                    @if($categories)
                        @foreach($categories as $cat_info)
                            <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}"
                                 id="{{ $cat_info->slug }}" role="tabpanel" aria-labelledby="{{ $cat_info->slug }}-tab">
                                @if($cat_info->food_categories)
                                    <div class="row">
                                        <div class="col-3 nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                             aria-orientation="vertical">
                                            @foreach($cat_info->food_categories as $food_cats)
                                                <a class="nav-link {{ $loop->first ?'active':'' }}"
                                                   id="v-pills-{{ $food_cats->slug }}-tab" data-toggle="pill"
                                                   href="#v-pills-{{ $food_cats->slug }}" role="tab"
                                                   aria-controls="v-pills-{{ $food_cats->slug }}"
                                                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $food_cats->title }}</a>
                                            @endforeach
                                        </div>
                                        <div class="col-9 tab-content" id="v-pills-tabContent">
                                            @foreach($cat_info->food_categories as $food_cats)
                                                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}"
                                                     id="v-pills-{{ $food_cats->slug }}" role="tabpanel"
                                                     aria-labelledby="v-pills-{{ $food_cats->slug }}-tab">
                                                    @if($food_cats->dishes)
                                                        <ul>
                                                            @foreach($food_cats->dishes as $dish_item)
                                                                <li class="dish-list" onclick="addOrder(this)"
                                                                    data-id="{{ $dish_item->id }}">
                                                                    <h6>{{ $dish_item->title }}</h6>
                                                                    <p>NPR. {{ number_format($dish_item->price) }}</p>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="row d-none" id="order-form">
                    <div class="col-12">
                        <hr>
                        <h5 class="text-center">Order Form</h5>
                        <hr>

                        {{ Form::open(['id'=>'form_order','url'=>'']) }}
                        <div class="form-group row">
                            {{ Form::label('quantity',"Quantity: ",['class'=>'col-sm-4']) }}
                            <div class="col-sm-4">
                                <input type="number" name="quantity" class="form-control input-number" value="1" min="1"
                                       max="100"></div>
                            <div class="col-sm-4">
                                {{ Form::hidden('table_id',$table_data->id) }}
                                {{ Form::button('Add OrderEvent',['class'=>'btn btn-success', 'id'=>'place_order','type'=>'submit']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="table_content"></div>
@endsection
