@php
    $subtotal = 0;
@endphp
<table class="table table-striped">
    <thead class="thead-dark">
    <th>Particular</th>
    <th>Quantity</th>
    <th>Price(Npr.)</th>
    <th>Amount(Npr.)</th>
    <th>Action</th>
    </thead>
    
    @if($orders->count() > 0)
        @foreach($orders as $order)
           
            <tr>
                <td>{{ $order->dish->title }}</td>
                <td>{{ $order->quantity }}</td>
                <td>{{ $order->unit_price }}</td>
                <td>{{ $order->unit_price * $order->quantity }}</td>
                @if($order->status == 'not_confirmed')
                    <td><a href="{{ route('delete-order', $order->id) }}" class="btn btn-danger" id="delete_order" onclick="deleteOrder(this)">Delete
                            Order</a></td>
                @else
                    <td>
                        <button disabled class="btn btn-success">Confirmed</button>
                    </td>
                @endif
                @php
                    $subtotal += $order->quantity * $order->unit_price
                @endphp
            </tr>
        @endforeach
    @endif
    @php
        $service_charge = $subtotal * 0.10;
        $vat = $subtotal * 0.13;
        $total = $subtotal + $vat + $service_charge;
    @endphp
    <tfoot>
    <tr>
        <th colspan="3" class="text-right">Sub-Total: NRS. {{ number_format($subtotal) }}</th>
        <th id="sub_total"></th>
    </tr>
    <tr>
        <th colspan="3" class="text-right">Service Charge(10%): NRS. {{ number_format($service_charge) }}</th>
        <th id="service_charge"></th>
    </tr>
    <tr>
        <th colspan="3" class="text-right">VAT (13%): NRS. {{ number_format($vat) }}</th>
        <th id="vat"></th>
    </tr>

    <tr>
        <th colspan="3" class="text-right">Total: NRS. {{ number_format($total) }}</th>
        <th id="total"></th>
    </tr>
    </tfoot>
</table>

