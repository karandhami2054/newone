<?php

use Illuminate\Database\Seeder;
use App\Models\Dish;

class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dish_items = array(
            array(
                'title' => 'Luger Burger',
                'slug' => 'luger-burger',
                'food_categories_id' => 1,
                'summary' => 'It takes a mighty burger for us not to order a steak at Peter Luger Steakhouse, and this is that burger. Served until 3:45pm daily, the Luger Burger features extra thick bacon (not an understatement) and more than a half pound of beef.',
                'price' => 350,
                'discount' => 0,
                'act_price' => 350,
                'status' => 'active'
            ),
            array(
                'title' => 'Le Pigeon Burger',
                'slug' => 'le-pigeon-burger',
                'food_categories_id' => 1,
                'summary' => 'The grilled Le Pigeon Burger has a nice, smoky flavor that is enhanced with each topping. Gabriel Rucker layers it with Tillamook white cheddar, lettuce slaw, and pickled onions along with some ketchup, mayo, and mustard. The ciabatta bun is also tremendous.',
                'price' => 250,
                'discount' => 30,
                'act_price' => 220,
                'status' => 'active'
            ),
            array(
                'title' => 'Double Animal Style',
                'slug' => 'double-animal-style',
                'food_categories_id' => 1,
                'summary' => 'OrderEvent this one off the secret menu that’s not-so-secret at all. Leveling up to “Animal Style” will give you added pickles, extra spread with grilled onions, and they’ll fry some mustard onto your patties.',
                'price' => 250,
                'discount' => 0,
                'act_price' => 250,
                'status' => 'active'
            ),
            array(
                'title' => 'The Original Burger',
                'slug' => 'the-original-burger',
                'food_categories_id' => 1,
                'summary' => 'Forget all the insane toppings and avant-garde techniques being used elsewhere, this is the way the burger was supposed to be. The place claims to have invented and served the first hamburger sandwich in the US back in 1900, and they send them out just the same way today as they did back then.',
                'price' => 190,
                'discount' => 0,
                'act_price' => 190,
                'status' => 'active'
            ),
            array(
                'title' => 'The Ham Burger',
                'slug' => 'the-ham-burger',
                'food_categories_id' => 1,
                'summary' => 'Forget all the insane toppings and avant-garde techniques being used elsewhere, this is the way the burger was supposed to be. The place claims to have invented and served the first hamburger sandwich in the US back in 1900, and they send them out just the same way today as they did back then.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'The Veg Burger',
                'slug' => 'the-veg-burger',
                'food_categories_id' => 1,
                'summary' => 'Forget all the insane toppings and avant-garde techniques being used elsewhere, this is the way the burger was supposed to be. The place claims to have invented and served the first hamburger sandwich in the US back in 1900, and they send them out just the same way today as they did back then.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'The Cheese Burger',
                'slug' => 'the-cheese-burger',
                'food_categories_id' => 1,
                'summary' => 'Forget all the insane toppings and avant-garde techniques being used elsewhere, this is the way the burger was supposed to be. The place claims to have invented and served the first hamburger sandwich in the US back in 1900, and they send them out just the same way today as they did back then.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Boiled Eggs',
                'slug' => 'boliled-eggs',
                'food_categories_id' => 2,
                'summary' => 'The original boiled egg returns',
                'price' => 20,
                'discount' => 0,
                'act_price' => 20,
                'status' => 'active'
            ),
            array(
                'title' => 'Plain Omlet',
                'slug' => 'plain-omlet',
                'food_categories_id' => 2,
                'summary' => 'The egg mashed in the hot oil in an frying pan.',
                'price' => 50,
                'discount' => 0,
                'act_price' => 50,
                'status' => 'active'
            ),
            array(
                'title' => 'Masala Omlet',
                'slug' => 'masala-omlet',
                'food_categories_id' => 2,
                'summary' => 'The egg mixed with masala and fried in frying pan.',
                'price' => 100,
                'discount' => 0,
                'act_price' => 100,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Sandwich',
                'slug' => 'veg-sandwich',
                'food_categories_id' => 3,
                'summary' => 'The sandwich made with veggie.',
                'price' => 120,
                'discount' => 0,
                'act_price' => 120,
                'status' => 'active'
            ),
            array(
                'title' => 'Cheese Sandwich',
                'slug' => 'cheese-sandwich',
                'food_categories_id' => 3,
                'summary' => 'The sandwich made with cheese.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Sandwich',
                'slug' => 'chicken-sandwich',
                'food_categories_id' => 3,
                'summary' => 'The sandwich made with chicken streaks.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Fresh Juice',
                'slug' => 'fresh-juice',
                'food_categories_id' => 4,
                'summary' => 'The freshly created juice grinded in the machine.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Canned Juice',
                'slug' => 'canned-juice',
                'food_categories_id' => 4,
                'summary' => 'The canned juice from Real in any taste.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Milk',
                'slug' => 'milk',
                'food_categories_id' => 4,
                'summary' => 'Fresh Milk.',
                'price' => 75,
                'discount' => 0,
                'act_price' => 75,
                'status' => 'active'
            ),
            array(
                'title' => 'Pancakes',
                'slug' => 'pancakes',
                'food_categories_id' => 5,
                'summary' => 'The Pancake.',
                'price' => 100,
                'discount' => 0,
                'act_price' => 100,
                'status' => 'active'
            ),
            array(
                'title' => 'Green Salad',
                'slug' => 'green-salad',
                'food_categories_id' => 6,
                'summary' => 'The green and very fresh salad.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'American Salad',
                'slug' => 'american-salad',
                'food_categories_id' => 6,
                'summary' => 'The American and very fresh salad.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Thali Set',
                'slug' => 'veg-thali-set',
                'food_categories_id' => 7,
                'summary' => 'The Nepali Thali set with rice, daal, veggies and dahi gheu.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Thali Set',
                'slug' => 'chicken-thali-set',
                'food_categories_id' => 7,
                'summary' => 'The thali set with chicken curry.',
                'price' => 250,
                'discount' => 0,
                'act_price' => 250,
                'status' => 'active'
            ),
            array(
                'title' => 'Mutton Thali Set',
                'slug' => 'mutton-thali-set',
                'food_categories_id' => 7,
                'summary' => 'The Mutton thali set.',
                'price' => 300,
                'discount' => 0,
                'act_price' => 300,
                'status' => 'active'
            ),
            array(
                'title' => 'Plain Rice',
                'slug' => 'plain-rice',
                'food_categories_id' => 7,
                'summary' => 'The plain rice.',
                'price' => 100,
                'discount' => 0,
                'act_price' => 100,
                'status' => 'active'
            ),
            array(
                'title' => 'Daal Fried',
                'slug' => 'daal-fried',
                'food_categories_id' => 8,
                'summary' => 'Daal Fried.',
                'price' => 120,
                'discount' => 0,
                'act_price' => 120,
                'status' => 'active'
            ),
            array(
                'title' => 'Plain Daal',
                'slug' => 'plain-daal',
                'food_categories_id' => 8,
                'summary' => 'Plain Daal Item.',
                'price' => 100,
                'discount' => 0,
                'act_price' => 100,
                'status' => 'active'
            ),
            array(
                'title' => 'Plain Roti',
                'slug' => 'plain-roti',
                'food_categories_id' => 9,
                'summary' => 'The Plain roti.',
                'price' => 10,
                'discount' => 0,
                'act_price' => 10,
                'status' => 'active'
            ),
            array(
                'title' => 'Plain Naan',
                'slug' => 'plain-naan',
                'food_categories_id' => 9,
                'summary' => 'The plain naan.',
                'price' => 30,
                'discount' => 0,
                'act_price' => 30,
                'status' => 'active'
            ),
            array(
                'title' => 'Butter Naan',
                'slug' => 'butter-naan',
                'food_categories_id' => 9,
                'summary' => 'The Butter naan.',
                'price' => 45,
                'discount' => 0,
                'act_price' => 45,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken keema Naan',
                'slug' => 'chicken-keema-naan',
                'food_categories_id' => 9,
                'summary' => 'The Chicken Keema naan.',
                'price' => 75,
                'discount' => 0,
                'act_price' => 75,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Keema Naan',
                'slug' => 'buff-keema-naan',
                'food_categories_id' => 9,
                'summary' => 'The Buff keema naan.',
                'price' => 65,
                'discount' => 0,
                'act_price' => 65,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Curry',
                'slug' => 'veg-curry',
                'food_categories_id' => 10,
                'summary' => 'Veg Curry.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Curry',
                'slug' => 'chicken-curry',
                'food_categories_id' => 10,
                'summary' => 'Chicken Curry.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Tikka Masala',
                'slug' => 'chicken-tikka-masala',
                'food_categories_id' => 10,
                'summary' => 'Chicken Tikka Masala.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Mutton Curry',
                'slug' => 'mutton-curry',
                'food_categories_id' => 10,
                'summary' => 'Mutton Curry.',
                'price' => 250,
                'discount' => 0,
                'act_price' => 250,
                'status' => 'active'
            ),
            array(
                'title' => 'Fish Curry',
                'slug' => 'fish-curry',
                'food_categories_id' => 10,
                'summary' => 'Fish Curry.',
                'price' => 250,
                'discount' => 0,
                'act_price' => 250,
                'status' => 'active'
            ),
            array(
                'title' => 'Mutton Fried',
                'slug' => 'mutton-fried',
                'food_categories_id' => 11,
                'summary' => 'Mutton Fried.',
                'price' => 300,
                'discount' => 0,
                'act_price' => 300,
                'status' => 'active'
            ),
            array(
                'title' => 'Mutton Sekuwa',
                'slug' => 'mutton-sekuwa',
                'food_categories_id' => 11,
                'summary' => 'Sekuwa mutton.',
                'price' => 350,
                'discount' => 0,
                'act_price' => 350,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Fried',
                'slug' => 'chicken-fried',
                'food_categories_id' => 12,
                'summary' => 'Fried Chicken.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Tikka',
                'slug' => 'chicken-tikka',
                'food_categories_id' => 12,
                'summary' => 'Chicken Tikka.',
                'price' => 220,
                'discount' => 0,
                'act_price' => 220,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Sadeko',
                'slug' => 'chicken-sadeko',
                'food_categories_id' => 12,
                'summary' => 'Sadeko Chicken.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Choyila',
                'slug' => 'chicken-choyila',
                'food_categories_id' => 12,
                'summary' => 'Chicken Choyila.',
                'price' => 220,
                'discount' => 0,
                'act_price' => 220,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken BBQ',
                'slug' => 'chicken-bbq',
                'food_categories_id' => 12,
                'summary' => 'Chicken BBQ.',
                'price' => 220,
                'discount' => 0,
                'act_price' => 220,
                'status' => 'active'
            ),
            array(
                'title' => 'Fish Fried',
                'slug' => 'fish-fry',
                'food_categories_id' => 13,
                'summary' => 'Fish Fry.',
                'price' => 250,
                'discount' => 0,
                'act_price' => 250,
                'status' => 'active'
            ),
            array(
                'title' => 'Traut',
                'slug' => 'traut',
                'food_categories_id' => 13,
                'summary' => 'Traut.',
                'price' => 350,
                'discount' => 0,
                'act_price' => 350,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Momo (Steam)',
                'slug' => 'buff-momo-steam',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, steam.',
                'price' => 120,
                'discount' => 0,
                'act_price' => 120,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Momo (Fried)',
                'slug' => 'buff-momo-fried',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, Fried.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Momo (C)',
                'slug' => 'buff-momo-c',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, C.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Momo (Kothey)',
                'slug' => 'buff-momo-kothey',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, kothey.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Buff Momo (Jhol)',
                'slug' => 'buff-momo-jhol',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, Jhol.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Momo (Steam)',
                'slug' => 'chicken-momo-steam',
                'food_categories_id' => 15,
                'summary' => 'Chicken momo, steam.',
                'price' => 120,
                'discount' => 0,
                'act_price' => 120,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Momo (Fried)',
                'slug' => 'chicken-momo-fried',
                'food_categories_id' => 15,
                'summary' => 'Chicken momo, Fried.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Momo (C)',
                'slug' => 'chicken-momo-c',
                'food_categories_id' => 15,
                'summary' => 'Chicken momo, C.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Momo (Kothey)',
                'slug' => 'chicken-momo-kothey',
                'food_categories_id' => 15,
                'summary' => 'Chicken momo, kothey.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Chicken Momo (Jhol)',
                'slug' => 'chicken-momo-jhol',
                'food_categories_id' => 15,
                'summary' => 'Buff momo, Jhol.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Momo (Steam)',
                'slug' => 'veg-momo-steam',
                'food_categories_id' => 15,
                'summary' => 'Veg momo, steam.',
                'price' => 120,
                'discount' => 0,
                'act_price' => 120,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Momo (Fried)',
                'slug' => 'veg-momo-fried',
                'food_categories_id' => 15,
                'summary' => 'Veg momo, Fried.',
                'price' => 150,
                'discount' => 0,
                'act_price' => 150,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Momo (C)',
                'slug' => 'veg-momo-c',
                'food_categories_id' => 15,
                'summary' => 'Veg momo, C.',
                'price' => 180,
                'discount' => 0,
                'act_price' => 180,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Momo (Kothey)',
                'slug' => 'veg-momo-kothey',
                'food_categories_id' => 15,
                'summary' => 'Veg momo, kothey.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            ),
            array(
                'title' => 'Veg Momo (Jhol)',
                'slug' => 'veg-momo-jhol',
                'food_categories_id' => 15,
                'summary' => 'Veg momo, Jhol.',
                'price' => 200,
                'discount' => 0,
                'act_price' => 200,
                'status' => 'active'
            )
        );


        foreach($dish_items as $items){
            $dish = new Dish();
            if($dish->where('slug',$items['slug'])->count() > 0){
                $dish = $dish->where('slug', $items['slug'])->first();
            }
            $dish->fill($items);
            $dish->save();
        }
    }
}
