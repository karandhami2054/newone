<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = array(
            array(
                'title' => 'Breakfast',
                'slug' => 'breakfast',
                'status' => 'active'
            ),
            array(
                'title' => 'Main Course',
                'slug' => 'maincourse',
                'status' => 'active'
            ),
            array(
                'title' => 'Drinks',
                'slug' => 'drinks',
                'status' => 'active'
            ),
            array(
                'title' => 'Dessert',
                'slug' => 'dessert',
                'status' => 'active'
            ),
            array(
                'title' => 'Snacks',
                'slug' => 'snacks',
                'status' => 'active'
            )
        );

        foreach($cats as $ind_cats){
            $category = new Category;
            if($category->where('slug',$ind_cats['slug'])->count() > 0){
                $category = $category->where('slug',$ind_cats['slug'])->first();
            }
            $category->fill($ind_cats);
            $category->save();
        }

    }
}
