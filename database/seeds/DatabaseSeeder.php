<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(FloorsTableSeeder::class);
        $this->call(TablesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(FoodCategoriesTableSeeder::class);
        $this->call(DishesTableSeeder::class);
    }
}
