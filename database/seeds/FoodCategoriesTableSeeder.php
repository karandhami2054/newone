<?php

use App\Models\FoodCategory;
use Illuminate\Database\Seeder;

class FoodCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat_lists = array(
            array(
                'title' => 'Burger',
                'slug' => 'burger',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Eggs',
                'slug' => 'eggs',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Sandwich',
                'slug' => 'sandwich',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Juices And Milk',
                'slug' => 'juices-and-milk',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Pancakes',
                'slug' => 'pancakes',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Toast',
                'slug' => 'toast',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Salad',
                'slug' => 'salad',
                'status' => 'active',
                'categories_id' => '1'
            ),
            array(
                'title' => 'Thali',
                'slug' => 'thali',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Daal Items',
                'slug' => 'daal-items',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Roti And Naans',
                'slug' => 'roti-and-naans',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Curry',
                'slug' => 'curry',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Mutton',
                'slug' => 'mutton',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Chicken',
                'slug' => 'chicken',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Fish',
                'slug' => 'fish',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Buff',
                'slug' => 'buff',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Momo',
                'slug' => 'momo',
                'status' => 'active',
                'categories_id' => '2'
            ),

            array(
                'title' => 'Fish',
                'slug' => 'fish',
                'status' => 'active',
                'categories_id' => '2'
            ),
            array(
                'title' => 'Juices',
                'slug' => 'juices',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Coffee',
                'slug' => 'coffee',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Tea',
                'slug' => 'tea',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Soft Drinks',
                'slug' => 'soft-drinks',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Hard Drinks',
                'slug' => 'hard-drinks',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Cocktails',
                'slug' => 'cocktails',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Mocktails',
                'slug' => 'mocktails',
                'status' => 'active',
                'categories_id' => '3'
            ),
            array(
                'title' => 'Cookie',
                'slug' => 'cookie',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Brownies',
                'slug' => 'brownies',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Cakes',
                'slug' => 'cakes',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Pastry',
                'slug' => 'pastry',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Doughnut',
                'slug' => 'doughnut',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Bread',
                'slug' => 'bread',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Frosting',
                'slug' => 'frosting',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Ice Cream',
                'slug' => 'ice-cream',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Cookie',
                'slug' => 'cookie',
                'status' => 'active',
                'categories_id' => '4'
            ),
            array(
                'title' => 'Fries',
                'slug' => 'fries',
                'status' => 'active',
                'categories_id' => '5'
            ),
            array(
                'title' => 'Rolls',
                'slug' => 'rolls',
                'status' => 'active',
                'categories_id' => '5'
            ),
            array(
                'title' => 'Cutlet',
                'slug' => 'cutlet',
                'status' => 'active',
                'categories_id' => '5'
            ),
            array(
                'title' => 'Pakoda',
                'slug' => 'pakoda',
                'status' => 'active',
                'categories_id' => '5'
            )
        );

        foreach($cat_lists as $food_cats){
            $food_categories = new FoodCategory();
            if($food_categories->where('slug',$food_cats['slug'])->count() >0){
                $food_categories = $food_categories->where('slug',$food_cats['slug'])->first();
            }
            $food_categories->fill($food_cats);
            $food_categories->save();
        }
    }
}
