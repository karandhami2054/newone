<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'name' => 'Admin Restro Book',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'admin',
                'status' => 'active'
            ),
            array(
                'name' => 'Waiter Restro Book',
                'email' => 'waiter@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'waiter',
                'status' => 'active'
            ), array(
                'name' => 'Kitchen Restro Book',
                'email' => 'kitchen@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'kitchen',
                'status' => 'active'
            ), array(
                'name' => 'Cashier Restro Book',
                'email' => 'cashier@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'cashier',
                'status' => 'active'
            ), array(
                'name' => 'Customer Restro Book',
                'email' => 'customer@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'customer',
                'status' => 'active'
            ), array(
                'name' => 'Admin Restro Book',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin123'),
                'role' => 'admin',
                'status' => 'active'
            ), array(
                'name' => 'Cashier One',
                'email' => 'cashierone@restrobook.com',
                'password' => Hash::make('cashierone123'),
                'role' => 'cashier',
                'status' => 'active'
            ),
            array(
                'name' => 'Waiter One',
                'email' => 'waiterone@restrobook.com',
                'password' => Hash::make('waiterone123'),
                'role' => 'waiter',
                'status' => 'active'
            ),
            array(
                'name' => 'Waiter Two',
                'email' => 'waitertwo@restrobook.com',
                'password' => Hash::make('waitertwo123'),
                'role' => 'waiter',
                'status' => 'active'
            ),
            array(
                'name' => 'Kitchen One',
                'email' => 'kitchenone@restrobook',
                'password' => Hash::make('kitchenone123'),
                'role' => 'kitchen',
                'status' => 'active'
            ),
            array(
                'name' => 'Kitchen Two',
                'email' => 'kitchentwo@restrobook',
                'password' => Hash::make('kitchentwo123'),
                'role' => 'kitchen',
                'status' => 'active'
            ),
            array(
                'name' => 'Customer One',
                'email' => 'customerone@restrobook',
                'password' => Hash::make('customerone123'),
                'role' => 'customer',
                'status' => 'active'
            ),
            array(
                'name' => 'Customer Two',
                'email' => 'customertwo@restrobook',
                'password' => Hash::make('customertwo123'),
                'role' => 'customer',
                'status' => 'active'
            )
        );

        foreach ($users as $user_info) {
            $user = new User();
            if ($user->where('email', $user_info['email'])->count() <= 0) {
                $user->fill($user_info);
                $user->save();
            }
        }
    }
}
