<?php

use App\Models\Floor;
use Illuminate\Database\Seeder;

class FloorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $floor_data = array(
            array(
                'name' => 'Ground Floor',
                'slug' => 'ground-floor',
                'status' => 'active'
            ),
            array(
                'name' => 'First Floor',
                'slug' => 'first-floor',
                'status' => 'active'
            ),
            array(
                'name' => 'Second Floor',
                'slug' => 'second-floor',
                'status' => 'active'
            ),
        );
        foreach($floor_data as $floor_info){
            $floor = new Floor();
            if($floor->where('slug',$floor_info['slug'])->count() <= 0){
                $floor->fill($floor_info);
                $floor->save();
            }
        }
    }
}
