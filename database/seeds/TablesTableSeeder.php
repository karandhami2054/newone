<?php

use App\Models\Table;
use Illuminate\Database\Seeder;

class TablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_data = array(
            array(
                'table_no' => 'Table 01',
                'floor_id' => 1,
                'capacity' => 4,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 02',
                'capacity' => 4,
                'floor_id' => 1,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 03',
                'capacity' => 8,
                'floor_id' => 1,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 04',
                'capacity' => 6,
                'floor_id' => 1,
                'status' => 'inactive'
            ),
            array(
                'table_no' => 'Table 05',
                'capacity' => 4,
                'floor_id' => 1,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 01',
                'floor_id' => 2,
                'capacity' => 3,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 02',
                'capacity' => 4,
                'floor_id' => 2,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 03',
                'capacity' => 6,
                'floor_id' => 2,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 04',
                'capacity' => 2,
                'floor_id' => 2,
                'status' => 'inactive'
            ),
            array(
                'table_no' => 'Table 05',
                'capacity' => 8,
                'floor_id' => 2,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 01',
                'floor_id' => 3,
                'capacity' => 1,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 02',
                'capacity' => 8,
                'floor_id' => 3,
                'status' => 'active'
            ),
            array(
                'table_no' => 'Table 03',
                'capacity' => 4,
                'floor_id' => 3,
                'status' => 'active'
            )
        );

        foreach($table_data as $data){
            $table = new Table();
            if($table->where('floor_id', $data['floor_id'])->where('table_no',$data['table_no'])->count() <= 0){
                $table->fill($data);
                $table->save();
            }
        }
    }
}
