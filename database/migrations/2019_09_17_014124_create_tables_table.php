<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('table_no');
            $table->unsignedBigInteger('floor_id')->nullable();
            $table->integer('capacity');
            $table->string('icon')->nullable();
            $table->enum('occupied',['occupied','reserved','open'])->default('open');
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->unsignedBigInteger('waiter_id')->nullable();
            $table->unsignedBigInteger('added_by')->nullable();
            $table->foreign('floor_id')->references('id')->on('floors')->onDelete('SET NULL');
            $table->foreign('added_by')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('waiter_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
