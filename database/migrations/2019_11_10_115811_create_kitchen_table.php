<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKitchenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dish_name');
            $table->unsignedBigInteger('dish_id');
            $table->unsignedBigInteger('table_id');
            $table->unsignedBigInteger('waiter_id');
            $table->string('order_code')->nullable();
            $table->unsignedBigInteger('order_id');
            $table->integer('quantity')->nullable();
            $table->string('status')->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('dish_id')->references('id')->on('dishes')->onDelete('NO ACTION')->onUpdate('CASCADE');
            $table->foreign('table_id')->references('id')->on('tables')->onDelete('NO ACTION')->onUpdate('CASCADE');
            $table->foreign('waiter_id')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('CASCADE');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchens');
    }
}
