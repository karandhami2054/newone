<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_code');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('waiter_id')->nullable();
            $table->float('sub_total');
            $table->float('discount')->nullable();
            $table->float('service_charge')->nullable();
            $table->float('vat')->nullable();
            $table->float('grand_total');
            $table->float('tips')->nullable();
            $table->enum('payment_mode',['cash','card','cheque','gateway','other'])->default('cash');
            $table->string('other_payment_mode')->nullable();
            $table->unsignedBigInteger('cashier_id')->nullable();
//            $table->foreign('order_code')->references('order_code')->on('orders')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('NO ACTION');
            $table->foreign('waiter_id')->references('id')->on('users')->onDelete('NO ACTION');
            $table->foreign('cashier_id')->references('id')->on('users')->onDelete('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
