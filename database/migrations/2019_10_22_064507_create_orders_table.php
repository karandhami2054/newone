<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_code')->index();
            $table->string('status')->nullable();
            $table->unsignedBigInteger('dish_id');
            $table->unsignedBigInteger('table_id');
            $table->integer('quantity');
            $table->unsignedBigInteger('waiter_id');
            $table->float('unit_price');
            $table->float('amount');
            $table->foreign('dish_id')->references('id')->on('dishes')->onDelete('NO ACTION')->onUpdate('CASCADE');
            $table->foreign('table_id')->references('id')->on('tables')->onDelete('NO ACTION')->onUpdate('CASCADE');
            $table->foreign('waiter_id')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
